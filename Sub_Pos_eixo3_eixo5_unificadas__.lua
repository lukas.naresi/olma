Sub Pos_eixo3_eixo5_unificadas()
'Lucas Romano 



 'zera ferramentas
 zera_ferramentas_posicoes
 
 'chama ferramentas
 FERRAMENTAS_eixo3
 
Dim largura
Set largura = SmartTags("BLANK_LARGURA")
'Declaração de todas as constantes 

SmartTags("BATIDA1_I") = 0
SmartTags("BATIDA2_I") = 0
SmartTags("BATIDA3_I") = 0

SmartTags("BATIDA1_L") = 0
SmartTags("BATIDA2_L") = 0
SmartTags("BATIDA3_L") = 0


'OFFSETS referente aos EIXO : 3-5
SmartTags("EIXO_OFFSET_A").Value = 4941000
SmartTags("EIXO_OFFSET_B").Value = 5441000
SmartTags("EIXO_OFFSET_C").Value = 5725000       
SmartTags("EIXO_OFFSET_D").Value = 5951000
SmartTags("EIXO_OFFSET_E").Value = 6186000
SmartTags("EIXO_OFFSET_F").Value = 6363500
SmartTags("EIXO_OFFSET_G").Value = 4905750
SmartTags("EIXO_OFFSET_H").Value = 5181500
SmartTags("EIXO_OFFSET_I").Value = 5406500
SmartTags("EIXO_OFFSET_L").Value = 5676500
SmartTags("EIXO_OFFSET_M").Value = 5946500


SmartTags("COTA1_B_TIPO_DIANTEIRO").Value = -48500
SmartTags("COTA1_C_TIPO_DIANTEIRO").Value = 65500
SmartTags("COTA1_D_TIPO_DIANTEIRO").Value = 8750
SmartTags("COTA1_E_TIPO_DIANTEIRO").Value = 301500
SmartTags("COTA1_F_TIPO_DIANTEIRO").Value = 124000
SmartTags("COTA1_I_TIPO_DIANTEIRO").Value = 32000
SmartTags("COTA2_I_TIPO_DIANTEIRO").Value = -13000
SmartTags("COTA_nula_1_TIPO_DIANTEIRO").Value = 0
SmartTags("COTA_nula_2_TIPO_DIANTEIRO").Value = 0
SmartTags("COTA_nula_3_TIPO_DIANTEIRO").Value = 0


SmartTags("COTA1_A_TIPO_TRASEIRO").Value = -212500
SmartTags("COTA1_B_TIPO_TRASEIRO").Value = -48500
SmartTags("COTA1_C_TIPO_TRASEIRO").Value = 65500
SmartTags("COTA1_G_TIPO_TRASEIRO").Value = 0
SmartTags("COTA1_I_TIPO_TRASEIRO").Value = 32000
SmartTags("COTA2_I_TIPO_TRASEIRO").Value = -12000
SmartTags("COTA1_L_TIPO_TRASEIRO").Value = 11300
SmartTags("COTA2_L_TIPO_TRASEIRO").Value = 0
SmartTags("COTA3_L_TIPO_TRASEIRO").Value = 11300
SmartTags("COTA_nula_1_TIPO_TRASEIRO").Value = 0


SmartTags("COTA1_B_TIPO_LATERAL").Value = -35000
SmartTags("COTA1_C_TIPO_LATERAL").Value = 52500
SmartTags("COTA1_I_TIPO_LATERAL").Value = 750
SmartTags("COTA2_I_TIPO_LATERAL").Value = 950
SmartTags("COTA3_I_TIPO_LATERAL").Value = 11000
SmartTags("COTA_nula_1_TIPO_LATERAL").Value = 0
SmartTags("COTA_nula_2_TIPO_LATERAL").Value = 0
SmartTags("COTA_nula_3_TIPO_LATERAL").Value = 0
SmartTags("COTA_nula_4_TIPO_LATERAL").Value = 0
SmartTags("COTA_nula_5_TIPO_LATERAL").Value = 0


'OFFSETS referente aos EIXO : 4
SmartTags("EIXO4_OFFSET_GERAL_DIANTEIRO").Value = -20000
SmartTags("EIXO4_OFFSET_A_TIPO_DIANTEIRO").Value = -20000
SmartTags("EIXO4_OFFSET_B_TIPO_DIANTEIRO").Value = -20000
SmartTags("EIXO4_OFFSET_C_TIPO_DIANTEIRO").Value = -20000  
SmartTags("EIXO4_OFFSET_D_TIPO_DIANTEIRO").Value = -20000
SmartTags("EIXO4_OFFSET_E_TIPO_DIANTEIRO").Value = -20000
SmartTags("EIXO4_OFFSET_F_TIPO_DIANTEIRO").Value = -20000
SmartTags("EIXO4_OFFSET_G_TIPO_DIANTEIRO").Value = -20600
SmartTags("EIXO4_OFFSET_H_TIPO_DIANTEIRO").Value = -20600
SmartTags("EIXO4_OFFSET_I_TIPO_DIANTEIRO").Value = -20600
SmartTags("EIXO4_OFFSET_L_TIPO_DIANTEIRO").Value = -20600
SmartTags("EIXO4_OFFSET_M_TIPO_DIANTEIRO").Value = -20600


SmartTags("EIXO4_OFFSET_GERAL_TRASEIRO").Value = -20000
SmartTags("EIXO4_OFFSET_A_TIPO_TRASEIRO").Value = -19900
SmartTags("EIXO4_OFFSET_B_TIPO_TRASEIRO").Value = -19000
SmartTags("EIXO4_OFFSET_C_TIPO_TRASEIRO").Value = -20600  
SmartTags("EIXO4_OFFSET_D_TIPO_TRASEIRO").Value = -20600
SmartTags("EIXO4_OFFSET_E_TIPO_TRASEIRO").Value = -20600
SmartTags("EIXO4_OFFSET_F_TIPO_TRASEIRO").Value = -20600
SmartTags("EIXO4_OFFSET_G_TIPO_TRASEIRO").Value = -19900
SmartTags("EIXO4_OFFSET_H_TIPO_TRASEIRO").Value = -20600
SmartTags("EIXO4_OFFSET_I_TIPO_TRASEIRO").Value = -19000
SmartTags("EIXO4_OFFSET_L_TIPO_TRASEIRO").Value = 42000
SmartTags("EIXO4_OFFSET_M_TIPO_TRASEIRO").Value = -20600


SmartTags("EIXO4_OFFSET_GERAL_LATERAL").Value = -20000
SmartTags("EIXO4_OFFSET_A_TIPO_LATERAL").Value = -20000
SmartTags("EIXO4_OFFSET_B_TIPO_LATERAL").Value = -20000
SmartTags("EIXO4_OFFSET_C_TIPO_LATERAL").Value = -20000  
SmartTags("EIXO4_OFFSET_D_TIPO_LATERAL").Value = -20000
SmartTags("EIXO4_OFFSET_E_TIPO_LATERAL").Value = -20000
SmartTags("EIXO4_OFFSET_F_TIPO_LATERAL").Value = -20000
SmartTags("EIXO4_OFFSET_G_TIPO_LATERAL").Value = -20000
SmartTags("EIXO4_OFFSET_H_TIPO_LATERAL").Value = -20000
SmartTags("EIXO4_OFFSET_I_TIPO_LATERAL").Value = -20000
SmartTags("EIXO4_OFFSET_L_TIPO_LATERAL").Value = -20000
SmartTags("EIXO4_OFFSET_M_TIPO_LATERAL").Value = -20000

'chamada da posição 2 pois é diferente da dos outros eixos

'POSICAO_2_eixo3

'Começo do programa que unifica todas as posições em uma unica função.

Dim ArrayF3(18) 
Dim ArrayPe3(18)
Dim ArrayPe4(18)
Dim ArrayPe43(17)
Dim ArrayF5(17)
Dim ArrayPe5(17)
Dim ArrayPe42(17)


'declaração dos itens da array de ferramenta do eixo 3
Set ArrayF3(1)=SmartTags("FERRAMENTA_2_Eixo3")
Set ArrayF3(2)=SmartTags("FERRAMENTA_3_Eixo3")
Set ArrayF3(3)=SmartTags("FERRAMENTA_4_Eixo3")
Set ArrayF3(4)=SmartTags("FERRAMENTA_5_Eixo3")
Set ArrayF3(5)=SmartTags("FERRAMENTA_6_Eixo3")
Set ArrayF3(6)=SmartTags("FERRAMENTA_7_Eixo3")
Set ArrayF3(7)=SmartTags("FERRAMENTA_8_Eixo3")
Set ArrayF3(8)=SmartTags("FERRAMENTA_9_Eixo3")
Set ArrayF3(9)=SmartTags("FERRAMENTA_10_Eixo3")
Set ArrayF3(10)=SmartTags("FERRAMENTA_11_Eixo3")
Set ArrayF3(11)=SmartTags("FERRAMENTA_12_Eixo3")
Set ArrayF3(12)=SmartTags("FERRAMENTA_13_Eixo3")
Set ArrayF3(13)=SmartTags("FERRAMENTA_14_Eixo3")
Set ArrayF3(14)=SmartTags("FERRAMENTA_15_Eixo3")
Set ArrayF3(15)=SmartTags("FERRAMENTA_16_Eixo3")
Set ArrayF3(16)=SmartTags("FERRAMENTA_17_Eixo3")
Set ArrayF3(17)=SmartTags("FERRAMENTA_18_Eixo3")
Set ArrayF3(18)=SmartTags("FERRAMENTA_19_Eixo3")

'declaração dos itens da array de posição teorica do eixo 3
Set ArrayPe3(1)=SmartTags("EIXO_3_T_POSITION_2")
Set ArrayPe3(2)=SmartTags("EIXO_3_T_POSITION_3")
Set ArrayPe3(3)=SmartTags("EIXO_3_T_POSITION_4")
Set ArrayPe3(4)=SmartTags("EIXO_3_T_POSITION_5")
Set ArrayPe3(5)=SmartTags("EIXO_3_T_POSITION_6")
Set ArrayPe3(6)=SmartTags("EIXO_3_T_POSITION_7")
Set ArrayPe3(7)=SmartTags("EIXO_3_T_POSITION_8")
Set ArrayPe3(8)=SmartTags("EIXO_3_T_POSITION_9")
Set ArrayPe3(9)=SmartTags("EIXO_3_T_POSITION_10")
Set ArrayPe3(10)=SmartTags("EIXO_3_T_POSITION_11")
Set ArrayPe3(11)=SmartTags("EIXO_3_T_POSITION_12")
Set ArrayPe3(12)=SmartTags("EIXO_3_T_POSITION_13")
Set ArrayPe3(13)=SmartTags("EIXO_3_T_POSITION_14")
Set ArrayPe3(14)=SmartTags("EIXO_3_T_POSITION_15")
Set ArrayPe3(15)=SmartTags("EIXO_3_T_POSITION_16")
Set ArrayPe3(16)=SmartTags("EIXO_3_T_POSITION_17")
Set ArrayPe3(17)=SmartTags("EIXO_3_T_POSITION_18")
Set ArrayPe3(18)=SmartTags("EIXO_3_T_POSITION_19")

'declaração dos itens da array de posição teorica do eixo 4 para lado 3
Set ArrayPe4(1)=SmartTags("EIXO_4_T_POSITION_2")
Set ArrayPe4(2)=SmartTags("EIXO_4_T_POSITION_3")
Set ArrayPe4(3)=SmartTags("EIXO_4_T_POSITION_4")
Set ArrayPe4(4)=SmartTags("EIXO_4_T_POSITION_5")
Set ArrayPe4(5)=SmartTags("EIXO_4_T_POSITION_6")
Set ArrayPe4(6)=SmartTags("EIXO_4_T_POSITION_7")
Set ArrayPe4(7)=SmartTags("EIXO_4_T_POSITION_8")
Set ArrayPe4(8)=SmartTags("EIXO_4_T_POSITION_9")
Set ArrayPe4(9)=SmartTags("EIXO_4_T_POSITION_10")
Set ArrayPe4(10)=SmartTags("EIXO_4_T_POSITION_11")
Set ArrayPe4(11)=SmartTags("EIXO_4_T_POSITION_12")
Set ArrayPe4(12)=SmartTags("EIXO_4_T_POSITION_13")
Set ArrayPe4(13)=SmartTags("EIXO_4_T_POSITION_14")
Set ArrayPe4(14)=SmartTags("EIXO_4_T_POSITION_15")
Set ArrayPe4(15)=SmartTags("EIXO_4_T_POSITION_16")
Set ArrayPe4(16)=SmartTags("EIXO_4_T_POSITION_17")
Set ArrayPe4(17)=SmartTags("EIXO_4_T_POSITION_18")
Set ArrayPe4(18)=SmartTags("EIXO_4_T_POSITION_19")


'declaração dos itens da array de posição teorica do eixo 4 para lado 5
Set ArrayPe42(1)=SmartTags("EIXO_4_T_POSITION_23")
Set ArrayPe42(2)=SmartTags("EIXO_4_T_POSITION_24")
Set ArrayPe42(3)=SmartTags("EIXO_4_T_POSITION_25")
Set ArrayPe42(4)=SmartTags("EIXO_4_T_POSITION_26")
Set ArrayPe42(5)=SmartTags("EIXO_4_T_POSITION_27")
Set ArrayPe42(6)=SmartTags("EIXO_4_T_POSITION_28")
Set ArrayPe42(7)=SmartTags("EIXO_4_T_POSITION_29")
Set ArrayPe42(8)=SmartTags("EIXO_4_T_POSITION_30")
Set ArrayPe42(9)=SmartTags("EIXO_4_T_POSITION_31")
Set ArrayPe42(10)=SmartTags("EIXO_4_T_POSITION_32")
Set ArrayPe42(11)=SmartTags("EIXO_4_T_POSITION_33")
Set ArrayPe42(12)=SmartTags("EIXO_4_T_POSITION_34")
Set ArrayPe42(13)=SmartTags("EIXO_4_T_POSITION_35")
Set ArrayPe42(14)=SmartTags("EIXO_4_T_POSITION_36")
Set ArrayPe42(15)=SmartTags("EIXO_4_T_POSITION_37")
Set ArrayPe42(16)=SmartTags("EIXO_4_T_POSITION_38")
Set ArrayPe42(17)=SmartTags("EIXO_4_T_POSITION_39")



'Declaração de valores do blank
Dim A, B, C

Set A = SmartTags("BLANK_COMPRIMENTO")
Set B = SmartTags("BLANK_LARGURA")
Set C = SmartTags("ALTURA_PROCESSO_C")

Dim F3,F5
Dim Pe3,Pe4,Pe42,Pe5,Pe43


Dim I
Dim TempArrayPe3(18) 
Dim TempArrayF3(18) 
Dim TempArrayPe5(18) 
Dim TempArrayF5(18) 
Dim TempArrayPe4(18)
Dim TempArrayPe42(17)


	Dim J 
    Dim Constante,Constante2
    Dim temp_p3
    Dim temp_f3
    Dim temp_p5
    Dim temp_f5
      
      


     If SmartTags("MODELO").Value = 1 Or SmartTags("MODELO").Value = 2 Then 
		    Constante = 6200000 
		    End If
		    
	 If SmartTags("MODELO").Value = 3  Then 
		    Constante = 5970000 
	 End If
	 
	 Constante2 = 9256000 - (largura/2) + 4000


' loop para cotas eixos 3-4

'modelo dianteiro

If MODELO = 1 Then
    For I = 1 To 18
		
		' BATIDAS 1x-2x-3x - LADO EIXO 3

'FERRAMENTA I
'Selecionado ferramenta I - 3x
If F3 = 256 And SmartTags("BATIDA2_I").Value =1 And SmartTags("BATIDA3_I").Value=0 Then
SmartTags("BATIDA3_I").Value = 1
End If
'Selecionado ferramenta I - 2x
If F3 = 256 And SmartTags("BATIDA1_I").Value =1 And SmartTags("BATIDA2_I").Value=0 Then
SmartTags("BATIDA2_I").Value = 1
End If


'FERRAMENTA L
'Selecionado ferramenta L - 3x
If F3 = 512 And SmartTags("BATIDA2_L").Value =1 And SmartTags("BATIDA3_L").Value=0 Then
SmartTags("BATIDA3_L").Value = 1
End If
'Selecionado ferramenta L - 2x
If F3 = 512 And SmartTags("BATIDA1_L").Value =1 And SmartTags("BATIDA2_L").Value=0 Then
SmartTags("BATIDA2_L").Value = 1
End If

		
		
        ' Definindo os valores das variáveis de acordo com as arrays
        F3 = ArrayF3(I)	
        Pe3 = ArrayPe3(I)
        Pe4 = ArrayPe4(I)
        

        ' Verificando a ferramenta selecionada e atualizando Pe3 e Pe4 conforme necessário
        Select Case F3
			Case 0
				Pe4= A.Value + SmartTags("EIXO4_OFFSET_GERAL_DIANTEIRO").Value
            Case 2
                Pe3 = SmartTags("EIXO_OFFSET_B").Value + SmartTags("COTA1_B_TIPO_DIANTEIRO").Value + B
                Pe4 = A.Value + SmartTags("EIXO4_OFFSET_B_TIPO_DIANTEIRO").Value
            Case 4
                Pe3 = SmartTags("EIXO_OFFSET_C").Value + SmartTags("COTA1_C_TIPO_DIANTEIRO").Value 
                Pe4 = A.Value + SmartTags("EIXO4_OFFSET_C_TIPO_DIANTEIRO").Value
            Case 8, 24, 56
                Pe3 = SmartTags("EIXO_OFFSET_D").Value + SmartTags("COTA1_D_TIPO_DIANTEIRO").Value + 527750
                Pe4 = A.Value + SmartTags("EIXO4_OFFSET_D_TIPO_DIANTEIRO").Value
            Case 16
                Pe3 = SmartTags("EIXO_OFFSET_E").Value + SmartTags("COTA1_E_TIPO_DIANTEIRO").Value 
                Pe4 = A.Value + SmartTags("EIXO4_OFFSET_E_TIPO_DIANTEIRO").Value
            Case 32
                Pe3 = SmartTags("EIXO_OFFSET_F").Value + SmartTags("COTA1_F_TIPO_DIANTEIRO").Value 
                Pe4 = A.Value + SmartTags("EIXO4_OFFSET_F_TIPO_DIANTEIRO").Value
			Case 256
				 Pe4 = A.Value + SmartTags("EIXO4_OFFSET_I_TIPO_DIANTEIRO").Value
				If  SmartTags("BATIDA1_I").Value = 0  Then 
                Pe3 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA1_I_TIPO_DIANTEIRO").Value 
               End If
                If SmartTags("BATIDA1_I").Value = 1 Then
                    Pe3 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA2_I_TIPO_DIANTEIRO").Value + B
                End If 
		End Select
		
		'Selecionado ferramenta L - 1x
If F3 = 512 And SmartTags("BATIDA1_L").Value =0 Then
SmartTags("BATIDA1_L").Value = 1
End If
		
'Selecionado ferramenta I - 1x
If F3 = 256 And SmartTags("BATIDA1_I").Value =0 Then
SmartTags("BATIDA1_I").Value = 1
End If		
		
		 ArrayPe3(I).value =  Pe3
		 ArrayPe4(I).value = Pe4
		 
	Next 
	
End If

'zera artificio das batidas para o proximo loop
SmartTags("BATIDA1_I") = 0
SmartTags("BATIDA2_I") = 0
SmartTags("BATIDA3_I") = 0

SmartTags("BATIDA1_L") = 0
SmartTags("BATIDA2_L") = 0
SmartTags("BATIDA3_L") = 0


'modelo traseiro


If MODELO = 2 Then
    For I = 1 To 18
		
		
		
				' BATIDAS 1x-2x-3x - LADO EIXO 3

'FERRAMENTA I
'Selecionado ferramenta I - 3x
If F3 = 256 And SmartTags("BATIDA2_I").Value =1 And SmartTags("BATIDA3_I").Value=0 Then
SmartTags("BATIDA3_I").Value = 1
End If
'Selecionado ferramenta I - 2x
If F3 = 256 And SmartTags("BATIDA1_I").Value =1 And SmartTags("BATIDA2_I").Value=0 Then
SmartTags("BATIDA2_I").Value = 1
End If


'FERRAMENTA L
'Selecionado ferramenta L - 3x
If F3 = 512 And SmartTags("BATIDA2_L").Value =1 And SmartTags("BATIDA3_L").Value=0 Then
SmartTags("BATIDA3_L").Value = 1
End If
'Selecionado ferramenta L - 2x
If F3 = 512 And SmartTags("BATIDA1_L").Value =1 And SmartTags("BATIDA2_L").Value=0 Then
SmartTags("BATIDA2_L").Value = 1
End If

		
		
		
		
		
        ' Definindo os valores das variáveis de acordo com as arrays
        F3 = ArrayF3(I)
        Pe3 = ArrayPe3(I)
        Pe4 = ArrayPe4(I)
        


        ' Verificando a ferramenta selecionada e atualizando Pe3 e Pe4 conforme necessário
        Select Case F3
			Case 0
				Pe4= A.Value + SmartTags("EIXO4_OFFSET_GERAL_TRASEIRO").Value
            Case 1, 65
                Pe3 = SmartTags("EIXO_OFFSET_A").Value + SmartTags("COTA1_A_TIPO_TRASEIRO").Value
                Pe4 = A.Value + SmartTags("EIXO4_OFFSET_A_TIPO_TRASEIRO").Value
            Case 2
                Pe3 = SmartTags("EIXO_OFFSET_B").Value + SmartTags("COTA1_B_TIPO_TRASEIRO").Value + B
                Pe4 = A.Value + SmartTags("EIXO4_OFFSET_B_TIPO_TRASEIRO").Value
            Case 4
                Pe3 = SmartTags("EIXO_OFFSET_C").Value + SmartTags("COTA1_C_TIPO_TRASEIRO").Value
                Pe4 = A.Value + SmartTags("EIXO4_OFFSET_C_TIPO_TRASEIRO").Value
            Case 64
                Pe3 = SmartTags("EIXO_OFFSET_G").Value + SmartTags("COTA1_G_TIPO_TRASEIRO").Value
                Pe4 = A.Value + SmartTags("EIXO4_OFFSET_G_TIPO_TRASEIRO").Value
			Case 256
				 Pe4 = A.Value + SmartTags("EIXO4_OFFSET_I_TIPO_TRASEIRO").Value
				If SmartTags("BATIDA1_I").Value = 0  Then
                Pe3 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA1_I_TIPO_TRASEIRO").Value
               End If
                If SmartTags("BATIDA1_I").Value = 1 Then
                    Pe3 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA2_I_TIPO_TRASEIRO").Value + B
                End If
			Case 512
				Pe4 = A.Value + SmartTags("EIXO4_OFFSET_L_TIPO_TRASEIRO").Value
				If SmartTags("BATIDA1_L").Value = 0 Then
                Pe3 = SmartTags("EIXO_OFFSET_L").Value + SmartTags("COTA1_L_TIPO_TRASEIRO").Value
                End If
                If SmartTags("BATIDA1_L").Value = 1 Then
                Pe3 = SmartTags("EIXO_OFFSET_L").Value + SmartTags("COTA2_L_TIPO_TRASEIRO").Value + (B / 2)
                End If
                If SmartTags("BATIDA2_L").Value = 1 Then
                Pe3 = SmartTags("EIXO_OFFSET_L").Value + SmartTags("COTA3_L_TIPO_TRASEIRO").Value + B
                End If
                
		End Select
		
'Selecionado ferramenta I - 1x
If F3 = 256 And SmartTags("BATIDA1_I").Value =0 Then
SmartTags("BATIDA1_I").Value = 1
End If


'Selecionado ferramenta L - 1x
If F3 = 512 And SmartTags("BATIDA1_L").Value =0 Then
SmartTags("BATIDA1_L").Value = 1
End If
		
		 ArrayPe3(I).value =  Pe3
		 ArrayPe4(I).value = Pe4
    Next 
End If

'zera artificio das batidas para o proximo loop
SmartTags("BATIDA1_I") = 0
SmartTags("BATIDA2_I") = 0
SmartTags("BATIDA3_I") = 0

SmartTags("BATIDA1_L") = 0
SmartTags("BATIDA2_L") = 0
SmartTags("BATIDA3_L") = 0

'modelo lateral

If MODELO = 3 Then
    For I = 1 To 18
		
		
			' BATIDAS 1x-2x-3x - LADO EIXO 3

'FERRAMENTA I
'Selecionado ferramenta I - 3x
If F3 = 256 And SmartTags("BATIDA2_I").Value =1 And SmartTags("BATIDA3_I").Value=0 Then
SmartTags("BATIDA3_I").Value = 1
End If
'Selecionado ferramenta I - 2x
If F3 = 256 And SmartTags("BATIDA1_I").Value =1 And SmartTags("BATIDA2_I").Value=0 Then
SmartTags("BATIDA2_I").Value = 1
End If


'FERRAMENTA L
'Selecionado ferramenta L - 3x
If F3 = 512 And SmartTags("BATIDA2_L").Value =1 And SmartTags("BATIDA3_L").Value=0 Then
SmartTags("BATIDA3_L").Value = 1
End If
'Selecionado ferramenta L - 2x
If F3 = 512 And SmartTags("BATIDA1_L").Value =1 And SmartTags("BATIDA2_L").Value=0 Then
SmartTags("BATIDA2_L").Value = 1
End If

	
		
		
		
		
        ' Definindo os valores das variáveis de acordo com as arrays
        F3 = ArrayF3(I)
        Pe3 = ArrayPe3(I)
        Pe4 = ArrayPe4(I)
        


        ' Verificando a ferramenta selecionada e atualizando Pe3 e Pe4 conforme necessário
        Select Case F3
			Case 0
				Pe4= A.Value + SmartTags("EIXO4_OFFSET_GERAL_LATERAL").Value
            Case 2
                Pe3 = SmartTags("EIXO_OFFSET_B").Value + SmartTags("COTA1_B_TIPO_LATERAL").Value + B
                Pe4 = A.Value + SmartTags("EIXO4_OFFSET_B_TIPO_LATERAL").Value
            Case 4
                Pe3 = SmartTags("EIXO_OFFSET_C").Value + SmartTags("COTA1_C_TIPO_LATERAL").Value
                Pe4 = A.Value + SmartTags("EIXO4_OFFSET_C_TIPO_LATERAL").Value
			Case 256
				Pe4 = A.Value + SmartTags("EIXO4_OFFSET_I_TIPO_LATERAL").Value
				If SmartTags("BATIDA1_I").Value = 0 Then
                Pe3 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA1_I_TIPO_LATERAL").Value
                End If
                If SmartTags("BATIDA1_I").Value = 1 Then
                Pe3 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA2_I_TIPO_LATERAL").Value + (B / 2)
				End If
                If SmartTags("BATIDA2_I").Value = 1 Then
                Pe3 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA3_I_TIPO_LATERAL").Value + B
                End If
                
		End Select
		
'Selecionado ferramenta I - 1x
If F3 = 256 And SmartTags("BATIDA1_I").Value =0 Then
SmartTags("BATIDA1_I").Value = 1
End If	

'Selecionado ferramenta L - 1x
If F3 = 512 And SmartTags("BATIDA1_L").Value =0 Then
SmartTags("BATIDA1_L").Value = 1
End If	

		 ArrayPe3(I).value =  Pe3
		 ArrayPe4(I).value = Pe4
		
    Next 
End If

'zera artificio das batidas para o proximo loop
SmartTags("BATIDA1_I") = 0
SmartTags("BATIDA2_I") = 0
SmartTags("BATIDA3_I") = 0

SmartTags("BATIDA1_L") = 0
SmartTags("BATIDA2_L") = 0
SmartTags("BATIDA3_L") = 0



' verificação dos limites do eixo 3 e movimentação para eixo 5

'declaração dos itens da array de ferramenta do eixo 5
Set ArrayF5(1)=SmartTags("FERRAMENTA_3_Eixo5")
Set ArrayF5(2)=SmartTags("FERRAMENTA_4_Eixo5")
Set ArrayF5(3)=SmartTags("FERRAMENTA_5_Eixo5")
Set ArrayF5(4)=SmartTags("FERRAMENTA_6_Eixo5")
Set ArrayF5(5)=SmartTags("FERRAMENTA_7_Eixo5")
Set ArrayF5(6)=SmartTags("FERRAMENTA_8_Eixo5")
Set ArrayF5(7)=SmartTags("FERRAMENTA_9_Eixo5")
Set ArrayF5(8)=SmartTags("FERRAMENTA_10_Eixo5")
Set ArrayF5(9)=SmartTags("FERRAMENTA_11_Eixo5")
Set ArrayF5(10)=SmartTags("FERRAMENTA_12_Eixo5")
Set ArrayF5(11)=SmartTags("FERRAMENTA_13_Eixo5")
Set ArrayF5(12)=SmartTags("FERRAMENTA_14_Eixo5")
Set ArrayF5(13)=SmartTags("FERRAMENTA_15_Eixo5")
Set ArrayF5(14)=SmartTags("FERRAMENTA_16_Eixo5")
Set ArrayF5(15)=SmartTags("FERRAMENTA_17_Eixo5")
Set ArrayF5(16)=SmartTags("FERRAMENTA_18_Eixo5")
Set ArrayF5(17)=SmartTags("FERRAMENTA_19_Eixo5")

'declaração dos itens da array de posição teorica do eixo 5
Set ArrayPe5(1)=SmartTags("EIXO_5_T_POSITION_3")
Set ArrayPe5(2)=SmartTags("EIXO_5_T_POSITION_4")
Set ArrayPe5(3)=SmartTags("EIXO_5_T_POSITION_5")
Set ArrayPe5(4)=SmartTags("EIXO_5_T_POSITION_6")
Set ArrayPe5(5)=SmartTags("EIXO_5_T_POSITION_7")
Set ArrayPe5(6)=SmartTags("EIXO_5_T_POSITION_8")
Set ArrayPe5(7)=SmartTags("EIXO_5_T_POSITION_9")
Set ArrayPe5(8)=SmartTags("EIXO_5_T_POSITION_10")
Set ArrayPe5(9)=SmartTags("EIXO_5_T_POSITION_11")
Set ArrayPe5(10)=SmartTags("EIXO_5_T_POSITION_12")
Set ArrayPe5(11)=SmartTags("EIXO_5_T_POSITION_13")
Set ArrayPe5(12)=SmartTags("EIXO_5_T_POSITION_14")
Set ArrayPe5(13)=SmartTags("EIXO_5_T_POSITION_15")
Set ArrayPe5(14)=SmartTags("EIXO_5_T_POSITION_16")
Set ArrayPe5(15)=SmartTags("EIXO_5_T_POSITION_17")
Set ArrayPe5(16)=SmartTags("EIXO_5_T_POSITION_18")
Set ArrayPe5(17)=SmartTags("EIXO_5_T_POSITION_19")



		  
'ordenação do eixo 3 antes de preencher os valores 0 por constante 
		

'inicializa as arrays temporarias

For I =1 To 18
	TempArrayPe3(I) =0
	TempArrayF3(I) =0
	TempArrayPe4(I) =0
Next

For I =1 To 18
	TempArrayPe5(I) =0
	TempArrayF5(I) =0
Next
	

' Move os valores maiores que "x" da ArrayPe3 e ArrayF3 para as arrays temporárias TempArrayPe5 e TempArrayF5
For I = 1 To 18
    If ArrayPe3(I) > Constante Then
'        TempArrayPe5(I) = ArrayPe3(I)' Move o valor da ArrayPe3 para TempArrayPe5
        TempArrayF5(I) = ArrayF3(I)' Move o valor correspondente da ArrayF3 para TempArrayF5
        ' Define o valor na ArrayPe3 como "x" e na ArrayF3 como 0
        TempArrayPe3(I) = Constante
        TempArrayF3(I) = 0
        TempArrayPe4(I) = A
        
	End If
	
	If ArrayPe3(I) < Constante Then
		TempArrayPe3(I) = ArrayPe3(I)
        TempArrayF3(I) =  ArrayF3(I)
'		TempArrayPe5(I) = Constante
		TempArrayF5(I) = 0
		TempArrayPe4(I) = ArrayPe4(I)
    End If
Next 

' Atualiza as SmartTags com os valores das novas arrays temporárias TempArrayPe3 e TempArrayF3
For I = 1 To 18
    ArrayPe3(I).Value = TempArrayPe3(I)
    ArrayF3(I).Value = TempArrayF3(I)
    ArrayPe4(I).value = TempArrayPe4(I)
Next 

' Preenche os zeros com valor constante
' Atualiza as SmartTags com os valores das novas arrays temporárias TempArrayPe3 e TempArrayF3
For I = 1 To 18
	
    If ArrayPe3(I) = 0 Then
    ArrayPe3(I).Value = Constante
	Else
	ArrayPe3(I).Value =	ArrayPe3(I).Value
	End If
	
	If ArrayPe4(I) = 0 Then
	ArrayPe4(I).value = A
	Else
	ArrayPe4(I).value = ArrayPe4(I).value
	End If
Next 



' Copia os valores da ArrayPe3 e da ArrayF3 para as novas arrays temporárias TempArrayPe3 e TempArrayF3
For I = 1 To 18
    TempArrayPe3(I) = ArrayPe3(I)
    TempArrayF3(I) = ArrayF3(I)
Next 


'tambem acompanha o eixo 4
Dim temp_pe4

' Bubble sort para ordenar a TempArrayPe3
For I = 1 To 17
    For J = 1 To 17 - I
        If TempArrayPe3(J) > TempArrayPe3(J + 1) Then
            ' Troca de elementos na TempArrayPe3
            temp_p3 = TempArrayPe3(J)
            TempArrayPe3(J) = TempArrayPe3(J + 1)
            TempArrayPe3(J + 1) = temp_p3

            ' Acompanha a movimentação na TempArrayF3
            temp_f3 = TempArrayF3(J)
            TempArrayF3(J) = TempArrayF3(J + 1)
            TempArrayF3(J + 1) = temp_f3
            
            'acompanha a movimentação na TempArrayPe4
            temp_pe4 = TempArrayPe4(J)
            TempArrayPe4(J) = TempArrayPe4(J + 1)
            TempArrayPe4(J + 1) = temp_pe4
           
            
        End If
    Next 
Next 



' Atualiza as SmartTags com os valores das novas arrays temporárias TempArrayPe3 e TempArrayF3
For I = 1 To 18
    ArrayPe3(I).Value = TempArrayPe3(I)
    ArrayF3(I).Value = TempArrayF3(I)
    ArrayPe4(I).value = TempArrayPe4(I)
Next   
    
'----------------------------------------------------------------------------------------------------




' Atualiza as SmartTags com os valores das arrays temporárias TempArrayPe5 e TempArrayF5
For I = 1 To 17
    	ArrayPe5(I).Value = TempArrayPe5(I)
   		ArrayF5(I).Value = TempArrayF5(I)
Next


'--------------------------------------------------------------------------------------------------------------


'ordenação do eixo 5 e remoção dos zeros



'COTAS EIXOS 5-4
'____________________________________________________________________________________________________________
' MODELO DIANTEIRO


If MODELO = 1 Then
    For I = 1 To 17
		
		
		'____________________________________________________________________________________________________________
' BATIDAS 1x-2x-3x - LADO EIXO 5

'FERRAMENTA I
'Selecionado ferramenta I - 3x
If F5 = 256 And SmartTags("BATIDA2_I").Value =1 And SmartTags("BATIDA3_I").Value=0 Then
SmartTags("BATIDA3_I").Value = 1
End If
'Selecionado ferramenta I - 2x
If F5 = 256 And SmartTags("BATIDA1_I").Value =1 And SmartTags("BATIDA2_I").Value=0 Then
SmartTags("BATIDA2_I").Value = 1
End If


'FERRAMENTA L
'Selecionado ferramenta L - 3x
If F5 = 512 And SmartTags("BATIDA2_L").Value =1 And SmartTags("BATIDA3_L").Value=0 Then
SmartTags("BATIDA3_L").Value = 1
End If
'Selecionado ferramenta L - 2x
If F5 = 512 And SmartTags("BATIDA1_L").Value =1 And SmartTags("BATIDA2_L").Value=0 Then
SmartTags("BATIDA2_L").Value = 1
End If

		
        ' Definindo os valores das variáveis de acordo com as arrays
        F5 = ArrayF5(I)
        Pe5 =  ArrayPe5(I)
        Pe42 = ArrayPe42(I)
        


        ' Verificando a ferramenta selecionada e atualizando Pe5 e Pe42 conforme necessário
        Select Case F5
			Case 0
				Pe42= A.Value + SmartTags("EIXO4_OFFSET_GERAL_DIANTEIRO").Value
            Case 2
                Pe5= SmartTags("EIXO_OFFSET_B").Value + SmartTags("COTA1_B_TIPO_DIANTEIRO").Value + B.Value -( B.Value + 4000)
                Pe42= A.Value + SmartTags("EIXO4_OFFSET_B_TIPO_DIANTEIRO").Value
            Case 4
                Pe5 = SmartTags("EIXO_OFFSET_C").Value + SmartTags("COTA1_C_TIPO_DIANTEIRO").Value -( B.Value + 4000)
                Pe42 = A.Value + SmartTags("EIXO4_OFFSET_C_TIPO_DIANTEIRO").Value
            Case 8, 24, 56
                Pe5 = SmartTags("EIXO_OFFSET_D").Value + SmartTags("COTA1_D_TIPO_DIANTEIRO").Value + 527750 -( B.Value + 4000)
                Pe42 = A.Value + SmartTags("EIXO4_OFFSET_D_TIPO_DIANTEIRO").Value
            Case 16
                Pe5 = SmartTags("EIXO_OFFSET_E").Value + SmartTags("COTA1_E_TIPO_DIANTEIRO").Value -( B.Value + 4000)
                Pe42 = A.Value + SmartTags("EIXO4_OFFSET_E_TIPO_DIANTEIRO").Value
            Case 32
                Pe5 = SmartTags("EIXO_OFFSET_F").Value + SmartTags("COTA1_F_TIPO_DIANTEIRO").Value -( B.Value + 4000)
                Pe42 = A.Value + SmartTags("EIXO4_OFFSET_F_TIPO_DIANTEIRO").Value
			Case 256
				Pe42 = A.Value + SmartTags("EIXO4_OFFSET_I_TIPO_DIANTEIRO").Value
				If SmartTags("BATIDA1_I").Value = 0 Then
				Pe5 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA2_I_TIPO_DIANTEIRO").Value + B.Value -( B.Value + 4000)	
					
                'Pe5= SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA1_I_TIPO_DIANTEIRO").Value -( B.Value + 4000)
				End If
				 
                If SmartTags("BATIDA1_I").Value = 1 Then
					
					
                    'Pe5 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA2_I_TIPO_DIANTEIRO").Value + B.Value -( B.Value + 4000)
				End If
				
		End Select
		
'Selecionado ferramenta I - 1x
If F5 = 256 And SmartTags("BATIDA1_I").Value =0 Then
SmartTags("BATIDA1_I").Value = 1
End If		

'Selecionado ferramenta L - 1x
If F5 = 512 And SmartTags("BATIDA1_L").Value =0 Then
SmartTags("BATIDA1_L").Value = 1
End If
		 ArrayPe5(I).value =  Pe5
		 ArrayPe42(I).value = Pe42
	Next 
	

End If



'zera artificio das batidas para o proximo Loop
SmartTags("BATIDA1_I") = 0
SmartTags("BATIDA2_I") = 0
SmartTags("BATIDA3_I") = 0

SmartTags("BATIDA1_L") = 0
SmartTags("BATIDA2_L") = 0
SmartTags("BATIDA3_L") = 0

'____________________________________________________________________________________________________________
' MODELO TRASEIRO

If MODELO = 2 Then
    For I = 1 To 17
		
				' BATIDAS 1x-2x-3x - LADO EIXO 5

'FERRAMENTA I
'Selecionado ferramenta I - 3x
If F5 = 256 And SmartTags("BATIDA2_I").Value =1 And SmartTags("BATIDA3_I").Value=0 Then
SmartTags("BATIDA3_I").Value = 1
End If
'Selecionado ferramenta I - 2x
If F5 = 256 And SmartTags("BATIDA1_I").Value =1 And SmartTags("BATIDA2_I").Value=0 Then
SmartTags("BATIDA2_I").Value = 1
End If


'FERRAMENTA L
'Selecionado ferramenta L - 3x
If F5 = 512 And SmartTags("BATIDA2_L").Value =1 And SmartTags("BATIDA3_L").Value=0 Then
SmartTags("BATIDA3_L").Value = 1
End If
'Selecionado ferramenta L - 2x
If F5 = 512 And SmartTags("BATIDA1_L").Value =1 And SmartTags("BATIDA2_L").Value=0 Then
SmartTags("BATIDA2_L").Value = 1
End If


		
		
        ' Definindo os valores das variáveis de acordo com as arrays
        F5 = ArrayF5(I)
        Pe5 =  ArrayPe5(I)
        Pe42 = ArrayPe42(I)
        

' adicionar o case 0
        ' Verificando a ferramenta selecionada e atualizando Pe5 e Pe42 conforme necessário
        Select Case F5
			Case 0
				Pe42= A.Value + SmartTags("EIXO4_OFFSET_GERAL_TRASEIRO").Value
            Case 1, 65
                Pe5 = SmartTags("EIXO_OFFSET_A").Value + SmartTags("COTA1_A_TIPO_TRASEIRO").Value - (B.Value + 4000)
                Pe42 = A.Value + SmartTags("EIXO4_OFFSET_A_TIPO_TRASEIRO").Value
            Case 2, 6
                Pe5 = SmartTags("EIXO_OFFSET_B").Value + SmartTags("COTA1_B_TIPO_TRASEIRO").Value + B.Value - (B.Value + 4000)
                Pe42 = A.Value + SmartTags("EIXO4_OFFSET_B_TIPO_TRASEIRO").Value
            Case 4
                Pe5 = SmartTags("EIXO_OFFSET_C").Value + SmartTags("COTA1_C_TIPO_TRASEIRO").Value - (B.Value + 4000)
                Pe42 = A.Value + SmartTags("EIXO4_OFFSET_C_TIPO_TRASEIRO").Value
            Case 64
                Pe5 = SmartTags("EIXO_OFFSET_G").Value + SmartTags("COTA1_G_TIPO_TRASEIRO").Value - (B.Value + 4000)
                Pe42 = A.Value + SmartTags("EIXO4_OFFSET_G_TIPO_TRASEIRO").Value
			Case 256
				Pe42 = A.Value + SmartTags("EIXO4_OFFSET_I_TIPO_TRASEIRO").Value
				
				
				If SmartTags("BATIDA1_I").Value = 0 Then
				Pe5 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA2_I_TIPO_TRASEIRO").Value + B.Value - (B.Value + 4000)	
					
                'Pe5 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA1_I_TIPO_TRASEIRO").Value - (B.Value + 4000)
				End If
				
                If SmartTags("BATIDA1_I").Value = 1 Then
					
				'Pe5 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA2_I_TIPO_TRASEIRO").Value + B.Value - (B.Value + 4000)
				End If
				
			Case 512
				 Pe42 = A.Value + SmartTags("EIXO4_OFFSET_L_TIPO_TRASEIRO").Value
				If   SmartTags("BATIDA1_L").Value = 0 And SmartTags("BATIDA2_L").Value = 0 Then
                Pe5 = SmartTags("EIXO_OFFSET_L").Value + SmartTags("COTA2_L_TIPO_TRASEIRO").Value + (B.Value / 2) - (B.Value + 4000)
                'valor original da logica
                'Pe5 = SmartTags("EIXO_OFFSET_L").Value + SmartTags("COTA1_L_TIPO_TRASEIRO").Value - (B.Value + 4000)
                End If
                If SmartTags("BATIDA1_L").Value = 1 Then
                Pe5= SmartTags("EIXO_OFFSET_L").Value + SmartTags("COTA3_L_TIPO_TRASEIRO").Value + B.Value - (B.Value + 4000)
                'valor original na logica
                'Pe5 = SmartTags("EIXO_OFFSET_L").Value + SmartTags("COTA2_L_TIPO_TRASEIRO").Value + (B.Value / 2) - (B.Value + 4000)
                End If
                If SmartTags("BATIDA2_L").Value = 1 Then
                'valor original da logica
                'Pe5= SmartTags("EIXO_OFFSET_L").Value + SmartTags("COTA3_L_TIPO_TRASEIRO").Value + B.Value - (B.Value + 4000)
                End If
                
		End Select
		
'Selecionado ferramenta I - 1x
If F5 = 256 And SmartTags("BATIDA1_I").Value =0 Then
SmartTags("BATIDA1_I").Value = 1
End If

'Selecionado ferramenta L - 1x
If F5 = 512 And SmartTags("BATIDA1_L").Value =0 Then
SmartTags("BATIDA1_L").Value = 1
End If

ArrayPe5(I).value =  Pe5
ArrayPe42(I).value = Pe42

		
    Next 
End If



'zera artificio das batidas para o proximo loop
SmartTags("BATIDA1_I") = 0
SmartTags("BATIDA2_I") = 0
SmartTags("BATIDA3_I") = 0

SmartTags("BATIDA1_L") = 0
SmartTags("BATIDA2_L") = 0
SmartTags("BATIDA3_L") = 0


'____________________________________________________________________________________________________________
' MODELO LATERAL

If MODELO = 3 Then
    For I = 1 To 17
		
		
		' BATIDAS 1x-2x-3x - LADO EIXO 5

'FERRAMENTA I
'Selecionado ferramenta I - 3x
If F5 = 256 And SmartTags("BATIDA2_I").Value =1 And SmartTags("BATIDA3_I").Value=0 Then
SmartTags("BATIDA3_I").Value = 1
End If
'Selecionado ferramenta I - 2x
If F5 = 256 And SmartTags("BATIDA1_I").Value =1 And SmartTags("BATIDA2_I").Value=0 Then
SmartTags("BATIDA2_I").Value = 1
End If


'FERRAMENTA L
'Selecionado ferramenta L - 3x
If F5 = 512 And SmartTags("BATIDA2_L").Value =1 And SmartTags("BATIDA3_L").Value=0 Then
SmartTags("BATIDA3_L").Value = 1
End If
'Selecionado ferramenta L - 2x
If F5 = 512 And SmartTags("BATIDA1_L").Value =1 And SmartTags("BATIDA2_L").Value=0 Then
SmartTags("BATIDA2_L").Value = 1
End If

		
		
		
		
        ' Definindo os valores das variáveis de acordo com as arrays
        F5 = ArrayF5(I)
        Pe5=  ArrayPe5(I)
        Pe42= ArrayPe42(I)
        


        ' Verificando a ferramenta selecionada e atualizando Pe5 e Pe42 conforme necessário
        Select Case F5
			Case 0
				Pe42= A.Value + SmartTags("EIXO4_OFFSET_GERAL_LATERAL").Value
            Case 2
                Pe5 = SmartTags("EIXO_OFFSET_B").Value + SmartTags("COTA1_B_TIPO_LATERAL").Value + B.Value  -( B.Value + 4000)
                Pe42 = A.Value + SmartTags("EIXO4_OFFSET_B_TIPO_LATERAL").Value
            Case 4
                Pe5 = SmartTags("EIXO_OFFSET_C").Value + SmartTags("COTA1_C_TIPO_LATERAL").Value -( B.Value + 4000)
                Pe42 = A.Value + SmartTags("EIXO4_OFFSET_C_TIPO_LATERAL").Value
			Case 256
				Pe42 = A.Value + SmartTags("EIXO4_OFFSET_I_TIPO_LATERAL").Value
				If SmartTags("BATIDA1_I").Value = 0 Then
				Pe5 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA2_I_TIPO_LATERAL").Value + (B.Value / 2) -( B.Value + 4000)	
					
                'Pe5 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA1_I_TIPO_LATERAL").Value -( B.Value + 4000)
				End If
				
                If SmartTags("BATIDA1_I").Value = 1 Then
				Pe5 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA3_I_TIPO_LATERAL").Value + B.Value -( B.Value + 4000)	
					
                 'Pe5 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA2_I_TIPO_LATERAL").Value + (B.Value / 2) -( B.Value + 4000)
				End If
				
                 If SmartTags("BATIDA2_I").Value = 1 Then
								 
                 Pe5 = SmartTags("EIXO_OFFSET_I").Value + SmartTags("COTA3_I_TIPO_LATERAL").Value + B.Value -( B.Value + 4000)
                  End If
                
		End Select
		
'Selecionado ferramenta I - 1x
If F5 = 256 And SmartTags("BATIDA1_I").Value =0 Then
SmartTags("BATIDA1_I").Value = 1
End If

'Selecionado ferramenta L - 1x
If F5 = 512 And SmartTags("BATIDA1_L").Value =0 Then
SmartTags("BATIDA1_L").Value = 1
End If				
		
		
		ArrayPe5(I).value =  Pe5
		 ArrayPe42(I).value = Pe42
    Next 
End If

'____________


'zera artificio das batidas para o proximo loop
SmartTags("BATIDA1_I") = 0
SmartTags("BATIDA2_I") = 0
SmartTags("BATIDA3_I") = 0

SmartTags("BATIDA1_L") = 0
SmartTags("BATIDA2_L") = 0
SmartTags("BATIDA3_L") = 0







'
''tratamento das arrays para remoção dos zeros e ordenação
'	
''ordenação do eixo 5 antes de preencher os valores 0 por constante 	

For I =1 To 17
	TempArrayPe42(I) =0
Next

' Copia os valores da ArrayPe5 e da ArrayF5 para as novas arrays temporárias
For J = 1 To 17
    TempArrayPe5(J) = ArrayPe5(J)
    TempArrayF5(J) = ArrayF5(J)
    TempArrayPe42(J) = ArrayPe42(J)
Next 

Dim temp_pe42

' Bubble sort para ordenar a ArrayPe5
For J = 1 To 17
    For I = 1 To 17 - J
        If TempArrayPe5(I) >  TempArrayPe5(I + 1) Then
            ' Troca de elementos na TempArrayPe5
            temp_p5 = TempArrayPe5(I)
            TempArrayPe5(I) = TempArrayPe5(I + 1)
            TempArrayPe5(I + 1) = temp_p5

            ' Acompanha a movimentação na TempArrayF5
            temp_f5 = TempArrayF5(I)
            TempArrayF5(I) = TempArrayF5(I + 1)
            TempArrayF5(I + 1) = temp_f5
            
            
            ' Acompanha a movimentação na TempArrayPE42
            temp_pe42 = TempArrayPe42(I)
            TempArrayPe42(I) = TempArrayPe42(I + 1)
            TempArrayPe42(I + 1) = temp_pe42
        End If
    Next 
Next 

' Atualiza as SmartTags com os valores das novas arrays temporárias
For J = 1 To 17
    ArrayPe5(J).Value = TempArrayPe5(J)
    ArrayF5(J).Value = TempArrayF5(J)
    ArrayPe42(J).value =TempArrayPe42(J)
Next 


' Preenche os zeros com valor constante
' Atualiza as SmartTags com os valores das novas arrays temporárias TempArrayPe3 e TempArrayF3
For I = 1 To 17
	
    If ArrayPe5(I) = 0 Then
    ArrayPe5(I).Value = Constante2
	Else
	ArrayPe5(I).Value =	ArrayPe5(I).Value
	
	If ArrayPe42(I) = 0 Then
	ArrayPe42(I).value = A
	Else
	ArrayPe42(I).value = ArrayPe42(I).value
		End If
		
    End If
Next 

' Copia os valores da ArrayPe5 e da ArrayF5 para as novas arrays temporárias
For J = 1 To 17
    TempArrayPe5(J) = ArrayPe5(J)
    TempArrayF5(J) = ArrayF5(J)
    TempArrayPe42(J) =ArrayPe42(J)
Next 

' Bubble sort para ordenar a ArrayPe5
For J = 1 To 17
    For I = 1 To 17 - J
        If TempArrayPe5(I) >  TempArrayPe5(I + 1) Then
            ' Troca de elementos na TempArrayPe5
            temp_p5 = TempArrayPe5(I)
            TempArrayPe5(I) = TempArrayPe5(I + 1)
            TempArrayPe5(I + 1) = temp_p5

            ' Acompanha a movimentação na TempArrayF5
            temp_f5 = TempArrayF5(I)
            TempArrayF5(I) = TempArrayF5(I + 1)
            TempArrayF5(I + 1) = temp_f5
            
            ' Acompanha a movimentação na TempArrayPE42
            temp_pe42 = TempArrayPe42(I)
            TempArrayPe42(I) = TempArrayPe42(I + 1)
            TempArrayPe42(I + 1) = temp_pe42
            
        End If
    Next 
Next 


' Atualiza as SmartTags com os valores das novas arrays temporárias
For J = 1 To 17
    ArrayPe5(J).Value = TempArrayPe5(J)
    ArrayF5(J).Value = TempArrayF5(J)
    ArrayPe42(J).value =TempArrayPe42(J)
Next



EIXO_6


End Sub